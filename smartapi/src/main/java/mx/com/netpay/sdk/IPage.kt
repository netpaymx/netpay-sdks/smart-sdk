package mx.com.netpay.sdk

import android.graphics.Bitmap
import android.graphics.Typeface

interface IPage  {

    val lines: List<ILine>
    var typeFace: Typeface?
    var lineSpaceAdjustment: Int
    fun addLine(): ILine
    fun createUnit(): ILine.IUnit
    fun adjustLineSpace(spacingAdd: Int)
    fun addLines(lines: List<ILine>)

    interface ILine {

        val units: List<IUnit>
        var topSpaceAdjustment: Int
        fun addUnit(unit: IUnit): ILine
        fun adjustTopSpace(spacingAdd: Int): ILine

        interface IUnit {

            var text: String
            var bitmap: Bitmap?
            var fontSize: Int
            var gravity: Int
            var textStyle: Int
            var weight: Float

            companion object {
                val NORMAL = Typeface.NORMAL
                val BOLD = Typeface.BOLD
                val ITALIC = Typeface.ITALIC
                val BOLD_ITALIC = Typeface.BOLD_ITALIC
                val UNDERLINE = 1 shl 4
            }
        }
    }
}
