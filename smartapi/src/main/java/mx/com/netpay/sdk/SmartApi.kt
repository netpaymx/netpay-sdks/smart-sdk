package mx.com.netpay.sdk

import android.content.Intent
import android.graphics.Bitmap
import mx.com.netpay.sdk.models.*

interface SmartApi {
    fun doTrans(request: BaseRequest)
    fun doTrans(request: BaseRequest, additionalData:IPage)
    fun doTrans(request: BaseRequest, logo:Bitmap)
    fun doTrans(request: BaseRequest, displayScreenTip: Boolean,disablePrintAnimation: Boolean = false)
    fun doTrans(request: BaseRequest, additionalData: IPage? = null, logo: Bitmap? = null, displayScreenTip: Boolean = false,disablePrintAnimation: Boolean = false)
    fun doSettlement(request: BaseRequest)
    fun createPage(): IPage
    fun enableNavigation(key : NavBarButtons, enable : Boolean)
    fun enableStatusBar(enable : Boolean)
    fun showNavBar(enable : Boolean)
    fun showStatusBar(enable : Boolean)
    fun readMagneticCardBand()
    fun getStoreID()
    fun doRead(request: ReadNFCRequest)
    fun doRead(request: ReadNFCAuthRequest)
    fun onResult(requestCode: Int, resultCode: Int, data: Intent): BaseResponse
    /*fun doTransWithTip(request: BaseRequest)*/
}

