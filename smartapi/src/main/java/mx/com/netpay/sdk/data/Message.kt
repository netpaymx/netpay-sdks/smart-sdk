package mx.com.netpay.sdk.data

import android.content.ContentValues
import android.content.Context
import android.database.Cursor
import android.net.Uri
import android.support.v4.content.ContentResolverCompat

object Message {

    private val BASE_CONTENT_URI = Uri.parse("content://mx.com.netpay.smartpos.providers.SmartContentProvider")
    private const val KEY_PRINT = "SMART_SDK_PRINT"
    const val COL_MESSAGE = KEY_PRINT
    private const val PATH_MESSAGE = "message"
    private val CONTENT_URI: Uri =
        BASE_CONTENT_URI.buildUpon().appendPath(PATH_MESSAGE).build()

    @JvmField
    var autoContext: Context? = null

    private val instance: Message by lazy {
        Message(autoContext!!)
    }

    @JvmStatic
    fun get(context: Context): Message {
        autoContext = context
        return instance
    }

    @JvmStatic
    fun set(context: Context, message: String){
        get(context).messageString = message
    }


    //This class will use content provider to access the shared preference and display the data
    class Message(private val context: Context) {

        var messageString: String = ""
            get() {
                val cursor: Cursor? = ContentResolverCompat.query(
                    context.contentResolver,
                    CONTENT_URI, arrayOf(COL_MESSAGE),
                    null, null, null, null
                )
                cursor?.apply {
                    if (moveToFirst()) {
                        return getString(1)
                    }
                }
                return ""
            }
            set(value) {
                field = value
                val values = ContentValues()
                values.put(COL_MESSAGE,value)
                context.contentResolver.insert(CONTENT_URI,values)
            }
    }



}
