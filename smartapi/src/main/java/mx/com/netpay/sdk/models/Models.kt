package mx.com.netpay.sdk.models

import android.os.Build
import android.os.Bundle
import com.google.gson.Gson
import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName
import com.netpay.pax.paxconnector.BuildConfig
import mx.com.netpay.sdk.IPage
import mx.com.netpay.sdk.exceptions.SmartApiException
import mx.com.netpay.sdk.utils.Constants
import java.io.Serializable

abstract class BaseRequest(val appId: String) {
    internal fun toBundle(bundle: Bundle): Bundle {
        bundle.putInt(Constants.ID_REPORT, getIdReport())
        bundle.putString(Constants.APP_ID, appId)
        bundle.putString(Constants.DATA, Gson().toJson(this))
        return bundle
    }

    internal abstract fun checkArgs()
    internal abstract fun getIdReport(): Int
}

class SaleRequest(
    appId: String,
    val amount: Double,
    val tip: Double? = null,
    val msi: Int? = null,
    @SerializedName(value = "waiter", alternate = ["waitressid"])
    val waiter: Int? = null,
    val settlementId: Int? = null,
    val settlementDate: String? = null,
    val table: String? = null,
    val folio: String? = null,
    val checkIn: Boolean? = false,
    val tableId: Boolean? = false,
    val additionalData: Any? = null,
    val traceability: Any? = null,
    val exchangeRateUsd: String? = null,
    val pendingAmount: String? = null,
    val storeId: String?= null
) : BaseRequest(appId) {

    override fun checkArgs() {
        if (amount !in 0.01..999999.99) {
            throw SmartApiException("Monto inválido")
        }
    }

    override fun getIdReport() = 0

}


class CancelRequest(
    appId: String,
    val orderId: String,
    val storeId: String?= null
) : BaseRequest(appId) {
    override fun checkArgs() {
        if (orderId.isEmpty()) {
            throw  SmartApiException("OrderId inválido")
        }
    }

    override fun getIdReport() = 2
}

class ReprintRequest(
    appId: String,
    val orderId: String,
    val storeId: String?= null,
    val folioId: String?= null
) : BaseRequest(appId) {
    override fun checkArgs() {
        if (orderId.isEmpty() && folioId.isNullOrEmpty()) {
            throw  SmartApiException("OrderId inválido Reprint Request")
        }
    }

    override fun getIdReport() = 3

}

class ReadNFCRequest(
    appId: String
) : BaseRequest(appId) {
    override fun checkArgs() {}
    override fun getIdReport() = 13
}

class ReadNFCAuthRequest(
    appId: String,
    var block:Int? = 0,
    val password: String?=null
) : BaseRequest(appId) {
    override fun checkArgs() {}
    override fun getIdReport() = 13
}


class PrintRequest(appId: String, val page: IPage) : BaseRequest(appId) {
    override fun checkArgs() {}
    override fun getIdReport() = 4

}

class NavigationRequest(appId: String,
                        val key : Int,
                        val enable : Boolean) : BaseRequest(appId){
    override fun checkArgs() { if(key == 0) {throw SmartApiException("Valor inválido")} }
    override fun getIdReport() = 5
}

class StatusBarRequest(appId: String,
                       val enable : Boolean) : BaseRequest(appId){
    override fun checkArgs() {}
    override fun getIdReport() = 6

}


class CheckStatusRequest(appId: String,
                         val folioId: String) : BaseRequest(appId) {
    override fun checkArgs() { if(folioId.isEmpty()) { throw  SmartApiException("Fólio inválido") } }
    override fun getIdReport()= 9
}


class SettlementRequest(appId: String,
                        val storeId: String?= null) : BaseRequest(appId) {
    override fun checkArgs() {}
    override fun getIdReport() = 10
}


open class BaseResponse(var success: Boolean, var message: String)
open class StoreResponse( success: Boolean,message: String,val storeId: String?) : BaseResponse(success, message)
open class SaleResponse(success: Boolean,
                        message: String,
                        val affiliation: String? = null,
                        val storeName: String? = null,
                        val reprintModule: String? = null,
                        val streetName: String? = null,
                        val cityName: String? = null,
                        val rrnNumber:String? = null,
                        val bankName: String? = null,
                        @SerializedName(value = "cardType", alternate = ["cardNature"])
                        @Expose(serialize = false)
                        val cardNature: String? = null,
                        val cardTypeName: String?   = null,
                        val moduleLote: String? = null,
                        val moduleCharge: String? = null,
                        @SerializedName(value = "terminalId", alternate = ["moduleTerminal"])
                        @Expose(serialize = false)
                        val moduleTerminal: String? = null,
                        val applicationLabel: String? = null,
                        @SerializedName(value = "customerName", alternate = ["moduleName"])
                        @Expose(serialize = false)
                        val moduleName: String? = null,
                        val preAuth: String? = null,
                        @Deprecated("Este valor está obsoleto, utilizar spanRoute", ReplaceWith("spanRoute"))
                        val cardNumber: String? = null,
                        val spanRoute: String? = null,
                        val authCode: String? = null,
                        val tipLessAmount: String? = null,
                        val tipAmount: String? = null,
                        val amount: String? = null,
                        val authAmount: String?=null,
                        @SerializedName(value = "aid", alternate = ["attribute12"])
                        @Expose(serialize = false)
                        val attribute12: String? = null,
                        @SerializedName(value = "arqc", alternate = ["attribute11"])
                        @Expose(serialize = false)
                        val attribute11: String? = null,
                        val cardExpDate: String? = null,
                        val orderId: String? = null,
                        val promotion: String? = null,
                        val rePrintMark: String? = null,
                        val rePrintDate: String? = null,
                        val hexSign: String? = null,
                        val transDate: String? = null,
                        val preStatus: Int? = null,
                        @SerializedName(value = "responseCode", alternate = ["code"])
                        @Expose(serialize = false)
                        val code: String? = null,
                        @SerializedName(value = "transactionCertificate", alternate = ["transactionCert"])
                        @Expose(serialize = false)
                        val transactionCert: String? = null,
                        val transType: String? = null,
                        val isQps: Int? = null,
                        val hasPin: Boolean? = null,
                        val folioNumber: String? = "",
                        val internalNumber: String? = "",
                        val tableId: String? = "",
                        var traceability: Any? = null,
                        var bin: String? = "",
                        var isRePrint:Boolean = false,
                        var storeId:String?= null,
                        var sdkVersion: String? =  BuildConfig.VERSION_NAME,
                        var transactionId:String?= null) : BaseResponse(success, message)


class ReprintResponse(success: Boolean,
                      message: String) : SaleResponse(success, message)

class CancelResponse(success: Boolean,
                     message: String) : SaleResponse(success, message)


class PrintResponse(
    success: Boolean,
    message: String
) : SaleResponse(success, message)



class CheckStatusResponse(success: Boolean, 
                          message: String) :
    SaleResponse(success, message)



    internal data class ReprintTicketResponse(

        val affiliation: String? = null,
        val storeName: String? = null,
        val reprintModule: String? = null,
        val streetName: String? = null,
        val cityName: String? = null,
        val bankName: String? = null,
        val rrnNumber:String? = null,
        @SerializedName(value = "cardType", alternate = ["cardNature"])
        @Expose(serialize = false)
        val cardNature: String? = null,
        val cardTypeName: String? = null,
        val moduleLote: String? = null,
        val moduleCharge: String? = null,
        @SerializedName(value = "terminalId", alternate = ["moduleTerminal"])
        @Expose(serialize = false)
        val moduleTerminal: String? = null,
        val applicationLabel: String? = null,
        @SerializedName(value = "customerName", alternate = ["moduleName"])
        @Expose(serialize = false)
        val moduleName: String? = null,
        val preAuth: String? = null,
        @Deprecated("Este valor está obsoleto, utilizar spanRoute", ReplaceWith("spanRoute"))
        val cardNumber: String? = null,
        val spanRoute: String? = null,
        val authCode: String? = null,
        val tipLessAmount: String? = null,
        val tipAmount: String? = null,
        val amount: String? = null,
        val authAmount: String? = null,
        @SerializedName(value = "aid", alternate = ["attribute12"])
        @Expose(serialize = false)
        val attribute12: String? = null,
        @SerializedName(value = "arqc", alternate = ["attribute11"])
        @Expose(serialize = false)
        val attribute11: String? = null,
        val cardExpDate: String? = null,
        val orderId: String? = null,
        val promotion: String? = null,
        val rePrintMark: String? = null,
        val rePrintDate: String? = null,
        val hexSign: String? = null,
        val transDate: String? = null,
        val preStatus: Int? = null,
        val message: String? = null,
        @SerializedName(value = "responseCode", alternate = ["code"])
        @Expose(serialize = false)
        val code: String? = null,
        @SerializedName(value = "transactionCertificate", alternate = ["transactionCert"])
        @Expose(serialize = false)
        val transactionCert: String? = null,
        val transType: String? = null,
        val isQps: Int? = null,
        val hasPin: Boolean? = null,
        val folioNumber: String? = "",
        val internalNumber: String? = "",
        val tableId: String? = "",
        var traceability: Any? = null,
        var bin: String? = "",
        var isRePrint:Boolean = false,
        var storeId:String?= null,
        val success: Boolean,
        val sdkVersion: String? =  BuildConfig.VERSION_NAME
    ) : Serializable


internal data class IncomingMessage(
    val idReport: Int,
    val data: Any?
)

object Constants {
    const val SALE_REQUEST = 100
    const val CANCEL_REQUEST = 102
    const val REPRINT_REQUEST = 103
    const val PRINT_REQUEST = 104
    const val NAVIGATION_REQUEST = 105
    const val STATUS_REQUEST = 106
    const val SHOW_NAVBAR_REQUEST = 107
    const val SHOW_STATBAR_REQUEST = 108
    const val CHECK_STATUS_REQUEST = 109
    const val SETTLEMENT_REQUEST = 110
    const val READ_MAGNETIC_REQUEST = 111
    const val GET_STORE_ID = 112
    const val READ_NFC_REQUEST = 113
    const val READ_REQUEST = 113
}

enum class NavBarButtons(val idReport: Int) {
    ALL(1),
    HOME(2),
    BACK(3),
    RECENT(4)
}

class ReadResponse(
    success: Boolean = false,
    message: String,
    val id: String = "",
    val code:String= "-1",
    val value:String  = ""
) : BaseResponse(success, message)


class ReadRequest(
    appId: String
) : BaseRequest(appId) {
    override fun checkArgs() {
    }

    override fun getIdReport() = 11

}