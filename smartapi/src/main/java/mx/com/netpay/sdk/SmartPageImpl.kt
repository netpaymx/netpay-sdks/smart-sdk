package mx.com.netpay.sdk


import android.content.Context
import android.graphics.Bitmap
import android.graphics.Typeface
import android.view.Gravity
import android.view.View

import mx.com.netpay.sdk.IPage.ILine.IUnit.Companion.NORMAL

import java.util.ArrayList

internal class SmartPageImpl(private val context: Context) {

    fun pageToBitmap(page: IPage, pageWidth: Int): Bitmap {
        val viewConverter = ViewConverter(this.context)
        val view: View
        view = viewConverter.page2View(viewConverter.context, page, page.lines, pageWidth)
        view.measure(
            View.MeasureSpec.makeMeasureSpec(View.MeasureSpec.UNSPECIFIED, View.MeasureSpec.UNSPECIFIED),
            View.MeasureSpec.makeMeasureSpec(View.MeasureSpec.UNSPECIFIED, View.MeasureSpec.UNSPECIFIED)
        )
        view.layout(0, 0, pageWidth, view.measuredHeight)
        view.buildDrawingCache()
        return view.drawingCache
    }

    fun createPage(): IPage = Page()

    internal inner class Page : IPage {

        override val lines: MutableList<IPage.ILine> = ArrayList()
        override var typeFace: Typeface? = null
        override var lineSpaceAdjustment = 0

        override fun addLine(): IPage.ILine {
            val theLastLine = Line()
            this.lines.add(theLastLine)
            return theLastLine
        }

        override fun createUnit(): IPage.ILine.IUnit = Unit()

        override fun adjustLineSpace(spacingAdd: Int) {
            this.lineSpaceAdjustment = spacingAdd
        }

        override fun addLines(lines: List<IPage.ILine>) {
            this.lines.addAll(lines)
        }

    }


    internal inner class Line : IPage.ILine {
        override val units: MutableList<IPage.ILine.IUnit> = ArrayList()
        override var topSpaceAdjustment: Int = '\uffff'.toInt()

        override fun addUnit(unit: IPage.ILine.IUnit): IPage.ILine {
            this.units.add(unit)
            return this
        }

        override fun adjustTopSpace(spacingAdd: Int): IPage.ILine {
            this.topSpaceAdjustment = spacingAdd
            return this
        }

    }

    internal inner class Unit : IPage.ILine.IUnit {
        override var text: String = " "
        override var bitmap: Bitmap? = null
        override var fontSize: Int = 20
        override var gravity: Int = Gravity.START
        override var textStyle: Int = NORMAL
        override var weight = 1.0f
    }
}


