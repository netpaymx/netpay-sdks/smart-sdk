package mx.com.netpay.sdk.exceptions
class SmartApiException(message: String) : Exception(message)