package mx.com.netpay.sdk

import android.app.Activity

object SmartApiFactory {
    fun createSmartApi(activity: Activity): SmartApi = SmartApiImpl(activity)
}