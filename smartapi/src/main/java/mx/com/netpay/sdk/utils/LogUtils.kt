package mx.com.netpay.sdk.utils

import android.util.Log
import com.netpay.pax.paxconnector.BuildConfig

internal object LogUtils {
    private val IS_DEBUG = BuildConfig.DEBUG

    fun e(tag: String, msg: Any) {
        Log.e(tag, "" + msg)
    }

    fun w(tag: String, msg: Any) {
        Log.w(tag, "" + msg)
    }

    fun i(tag: String, msg: Any) {
        Log.i(tag, "" + msg)
    }

    fun d(tag: String, msg: Any) {
        if (msg.toString().length > 4000) {
            Log.d(tag, msg.toString().substring(0, 4000));
            d(tag, msg.toString().substring(4000))
        } else
            Log.d(tag, "" + msg)
    }

    fun v(tag: String, msg: Any) {
        Log.v(tag, "" + msg)
    }

    fun e(tag: String, msg: String, th: Throwable) {
        Log.e(tag, msg, th)
    }

    fun w(tag: String, msg: String, th: Throwable) {
        Log.w(tag, msg, th)
    }

    fun i(tag: String, msg: String, th: Throwable) {
        Log.i(tag, msg, th)
    }

    fun d(tag: String, msg: String, th: Throwable) {
        Log.d(tag, msg, th)
    }

    fun v(tag: String, msg: String, th: Throwable) {
        Log.v(tag, msg, th)
    }

    fun ew(tag: String, msg: Any) {
        Log.e(tag, "" + msg)
    }
}