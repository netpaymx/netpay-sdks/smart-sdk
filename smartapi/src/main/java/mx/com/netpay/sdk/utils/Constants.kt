package mx.com.netpay.sdk.utils

internal object Constants {
    val ID_REPORT = "ID_REPORT"
    val APP_ID = "APP_ID"
    val APP_PACKAGE = "APP_PACKAGE"
    val DATA = "DATA"
    val REQUEST_CODE = 100
    val PACKAGE_NAME = "mx.com.netpay.smartpos.dev"
    val URI = "smart://mx.com.netpay.smart.entry"
    val ACTION = "mx.com.netpay.smart.entry"
    val URINFC = "smart://mx.com.netpay.smart.entry.nfc"
    val ACTIONNFC = "mx.com.netpay.smart.entry.nfc"
}
