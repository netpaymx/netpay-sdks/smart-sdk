package mx.com.netpay.sdk;

import android.content.Context;
import android.graphics.Bitmap;
import android.text.SpannableString;
import android.text.style.AbsoluteSizeSpan;
import android.text.style.StyleSpan;
import android.text.style.UnderlineSpan;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ScrollView;
import android.widget.TextView;
import mx.com.netpay.sdk.IPage.ILine;
import mx.com.netpay.sdk.IPage.ILine.IUnit;

import java.util.Iterator;
import java.util.List;

class ViewConverter {
    private Context mContext;

    ViewConverter(Context context) {
        this.mContext = context;
    }

    Context getContext() {
        return this.mContext;
    }

    final View page2View(Context context, IPage page, List<ILine> lines, int pageWidth) {
        ScrollView scrollView = new ScrollView(context);
        LinearLayout.LayoutParams lpScrollView = new LinearLayout.LayoutParams(pageWidth, -2);
        scrollView.setLayoutParams(lpScrollView);
        scrollView.setBackgroundColor(-1);
        LinearLayout linearLayout = new LinearLayout(context);
        linearLayout.setLayoutParams(new LinearLayout.LayoutParams(pageWidth, -2));
        linearLayout.setOrientation(LinearLayout.VERTICAL);
        linearLayout.setBackgroundColor(-1);
        int width = 0;
        Iterator var9 = lines.iterator();

        while(var9.hasNext()) {
            ILine line = (ILine)var9.next();
            ++width;
            LinearLayout lineLayout = new LinearLayout(context);
            lineLayout.setLayoutParams(new LinearLayout.LayoutParams(-1, -2));
            lineLayout.setOrientation(LinearLayout.HORIZONTAL);
            lineLayout.setGravity(16);
            Iterator var12 = line.getUnits().iterator();

            while(true) {
                while(var12.hasNext()) {
                    IUnit unit = (IUnit)var12.next();
                    float weight = unit.getWeight();
                    Bitmap bitmap = unit.getBitmap();
                    String text = unit.getText();
                    if (text != null && text.length() > 0) {
                        TextView textView = new TextView(context);
                        textView.setLayoutParams(new LinearLayout.LayoutParams(0, -2, weight));
                        if (page.getTypeFace() != null) {
                            try {
                                textView.setTypeface(page.getTypeFace());
                            } catch (Exception var20) {
                                Log.e("ViewConverter", "", var20);
                            }
                        }

                        SpannableString ss = new SpannableString(unit.getText());
                        ss.setSpan(new AbsoluteSizeSpan(unit.getFontSize()), 0, ss.toString().length(), 33);
                        ss.setSpan(new StyleSpan(unit.getTextStyle() & 15), 0, ss.toString().length(), 33);
                        if ((unit.getTextStyle() & 240) == 16) {
                            ss.setSpan(new UnderlineSpan(), 0, ss.toString().length(), 33);
                        }

                        textView.setText(ss);
                        textView.setTextColor(-16777216);
                        textView.setTextSize((float)unit.getFontSize());
                        textView.setGravity(unit.getGravity());
                        lineLayout.addView(textView);
                    } else if (bitmap != null) {
                        ImageView imageView = new ImageView(context);
                        LinearLayout.LayoutParams lpImageView = new LinearLayout.LayoutParams(bitmap.getWidth(), bitmap.getHeight(), 0.0F);
                        lpImageView.setMargins(0, 10, 0, 10);
                        imageView.setLayoutParams(lpImageView);
                        imageView.setScaleType(ImageView.ScaleType.FIT_CENTER);
                        lineLayout.setGravity(unit.getGravity());
                        imageView.setImageBitmap(bitmap);
                        lineLayout.addView(imageView);
                    }
                }

                lineLayout.setLayoutDirection(View.LAYOUT_DIRECTION_LOCALE);
                linearLayout.addView(lineLayout);
                if (width != 1) {
                    int spaceAdjustment = page.getLineSpaceAdjustment();
                    int topSpaceAdjustment;
                    if ((topSpaceAdjustment = line.getTopSpaceAdjustment()) != 65535) {
                        spaceAdjustment = topSpaceAdjustment;
                    }

                    LinearLayout.LayoutParams layoutParams = (LinearLayout.LayoutParams)lineLayout.getLayoutParams();
                    layoutParams.topMargin = spaceAdjustment;
                    lineLayout.setLayoutParams(layoutParams);
                }
                break;
            }
        }

        scrollView.addView(linearLayout);
        return scrollView;
    }
}

