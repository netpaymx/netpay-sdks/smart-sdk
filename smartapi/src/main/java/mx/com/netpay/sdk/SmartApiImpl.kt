package mx.com.netpay.sdk

import android.app.Activity
import android.content.ActivityNotFoundException
import android.content.Intent
import android.content.pm.PackageManager
import android.content.pm.ResolveInfo
import android.net.Uri
import com.google.gson.Gson
import mx.com.netpay.sdk.models.*
import mx.com.netpay.sdk.models.ReprintTicketResponse
import mx.com.netpay.sdk.utils.Constants
import mx.com.netpay.sdk.utils.LogUtils
import android.graphics.Bitmap
import android.graphics.Canvas
import android.graphics.Color
import android.os.Build
import android.util.Base64
import android.widget.Toast
import com.netpay.pax.paxconnector.BuildConfig
import mx.com.netpay.sdk.data.Message
import org.json.JSONObject
import java.io.ByteArrayOutputStream

import java.lang.IllegalArgumentException
import kotlin.Exception
import kotlin.math.ceil


internal class SmartApiImpl(private val activity: Activity): SmartApi {

    private val smartPage = SmartPageImpl(activity)
    private val PAGE_WIDTH = if (Build.MODEL.contains("SK")) 576 else 350
    val DATACOLECT = "dataColect"
    companion object{
        const val SALE_WEB = mx.com.netpay.sdk.models.Constants.SALE_REQUEST
        const val CANCEL_SALE_WEB = mx.com.netpay.sdk.models.Constants.CANCEL_REQUEST
        const val REPRINT_SALE_WEB = mx.com.netpay.sdk.models.Constants.REPRINT_REQUEST
        const val PRINT_SALE = mx.com.netpay.sdk.models.Constants.PRINT_REQUEST
        const val ENABLE_NAVIGATION = mx.com.netpay.sdk.models.Constants.NAVIGATION_REQUEST
        const val ENABLE_STATUS_BAR = mx.com.netpay.sdk.models.Constants.STATUS_REQUEST
        const val SHOW_NAV_BAR = mx.com.netpay.sdk.models.Constants.SHOW_NAVBAR_REQUEST
        const val SHOW_STATUS_BAR = mx.com.netpay.sdk.models.Constants.SHOW_STATBAR_REQUEST
        const val CHECK_STATUS = mx.com.netpay.sdk.models.Constants.CHECK_STATUS_REQUEST
        const val SETTLEMENT_REQUEST = mx.com.netpay.sdk.models.Constants.SETTLEMENT_REQUEST
        const val READ_MAGNETIC_REQUEST = mx.com.netpay.sdk.models.Constants.READ_MAGNETIC_REQUEST
        const val READ_NFC_REQUEST = mx.com.netpay.sdk.models.Constants.READ_NFC_REQUEST
        const val GET_STORE_ID = mx.com.netpay.sdk.models.Constants.GET_STORE_ID
        const val MAX_LINES = 55
        const val READ_M_OPTON = 11
    }

    override fun doSettlement(request: BaseRequest) {
        doTrans(request)
    }

    override fun doTrans(request: BaseRequest) {
        doTrans(request, null, null)
    }

    override fun doTrans(request: BaseRequest, additionalData: IPage) {
        doTrans(request, additionalData, null)
    }


    override fun doTrans(request: BaseRequest, logo: Bitmap) {
        doTrans(request,null, logo)
    }

    override fun doTrans(request: BaseRequest, displayScreenTip: Boolean,disablePrintAnimation: Boolean){
        doTrans(request, null, null, displayScreenTip,disablePrintAnimation)
    }

    override fun doTrans(request: BaseRequest, additionalData: IPage?, logo: Bitmap?, displayScreenTip: Boolean,disablePrintAnimation: Boolean) {
        request.checkArgs()
        val uri = Uri.parse(Constants.URI)
        val intent = Intent(Constants.ACTION)
        intent.data = uri
        LogUtils.d("disablePrintAnimation-doTrans",disablePrintAnimation)
        val incomingMessage = if (request is PrintRequest) {
            if(request.page.lines.size > MAX_LINES){
                processPages(partition(request.page), request.getIdReport())
            }else{
                val bmp = smartPage.pageToBitmap(request.page, PAGE_WIDTH)
                val stream = ByteArrayOutputStream()
                bmp.compress(Bitmap.CompressFormat.PNG, 100, stream)
                val byteArray = stream.toByteArray()
                bmp.recycle()
                IncomingMessage(request.getIdReport(), String(Base64.encode(byteArray, Base64.DEFAULT)))
            }
        } else {
            IncomingMessage(request.getIdReport(), request)
        }

        val message = Gson().toJson(incomingMessage)
        Message.set(activity,message)
        val requestCode = request.getIdReport() + 100
        val activities: List<ResolveInfo> = activity.packageManager.queryIntentActivities(
            intent,
            PackageManager.MATCH_DEFAULT_ONLY
        )
        val isIntentSafe: Boolean = activities.isNotEmpty()
        if (isIntentSafe) {
            additionalData?.apply {
                intent.putExtra("INCOMING_MESSAGE_ADDITIONAL_DATA",pageToBase64(this))
            }

            
            logo?.apply {
                intent.putExtra("INCOMING_MESSAGE_LOGO",bitmapToBase64(logo))
            }
            if(displayScreenTip){
                intent.putExtra("displayScreenTip", displayScreenTip)
            }

            intent.putExtra("disablePrintAnimation", disablePrintAnimation)
            LogUtils.d("disablePrintAnimation-doTrans-Intent",disablePrintAnimation)
            activity.startActivityForResult(intent, requestCode)
            activity.overridePendingTransition(0, 0)
        } else {
            Toast.makeText(activity, "No se encontro la aplicación de pagos", Toast.LENGTH_LONG)
                .show()
        }
    }

    private fun partition(pageLines: IPage): ArrayList<IPage> {
        val listOfPages = ArrayList<IPage>()
        val size = pageLines.lines.size
        val typeFace = pageLines.typeFace
        val lineSpaceAdjustment = pageLines.lineSpaceAdjustment
        val divides = ceil(size / MAX_LINES.toDouble()).toInt()
        for (i in 0 until divides) {
            val start = i * MAX_LINES
            val end = if ((start + MAX_LINES) > size) size else start + MAX_LINES
            val page = createPage().apply {
                addLines(pageLines.lines.subList(start,end))
                this.typeFace = typeFace
                this.lineSpaceAdjustment = lineSpaceAdjustment
            }
            listOfPages.add(page)
        }
        return listOfPages
    }

    private fun processPages(pages: ArrayList<IPage>, idReport: Int): IncomingMessage{
        val total = pages.size
        val images = arrayListOf<Bitmap>()
        for (i in 0 until total) {
            val page = pages[i] as? IPage
            page?.apply {
                val bmp = smartPage.pageToBitmap(this, PAGE_WIDTH)
                images.add(bmp)
            }
        }
        if(images.isNotEmpty()){
            val mergedBitmap = mergeBitmaps(images)
            val stream = ByteArrayOutputStream()
            mergedBitmap.compress(Bitmap.CompressFormat.PNG, 100, stream)
            val byteArray = stream.toByteArray()
            mergedBitmap.recycle()
            return IncomingMessage(idReport, Base64.encodeToString(byteArray, Base64.DEFAULT))
        }else{
            throw IllegalArgumentException("ArrayList<*> must be not empty of IPages")
        }
    }

    private fun mergeBitmaps(images: ArrayList<Bitmap>): Bitmap {
        var bitmapHeight = 0
        if (images.isEmpty()) {
            throw IllegalArgumentException("list of bitmaps can not be empty")
        }
        for (bitmap in images) {
            bitmapHeight += bitmap.height
        }
        val result = Bitmap.createBitmap(images[0].width, bitmapHeight, images[0].config)
        val canvas = Canvas(result)
        var startOfBitmap = 0f
        for (bitmap in images) {
            canvas.drawBitmap(bitmap, 0f, startOfBitmap, null)
            startOfBitmap += bitmap.height.toFloat()
        }
        return result
    }

    override fun onResult(requestCode: Int, resultCode: Int, data: Intent): BaseResponse {
        val responseNFC = data.extras?.getString(DATACOLECT)
        LogUtils.d("onResult-sdk",responseNFC.toString())
        val response = data.extras?.getString("data")
        //LogUtils.d("Ibarra",response.toString())
        val incomingMessage: IncomingMessage? = Gson().fromJson(response, IncomingMessage::class.java)
        val ticket = Gson().fromJson(Gson().toJson(incomingMessage?.data), ReprintTicketResponse::class.java)
        val success = ticket?.code?.toIntOrNull() == 0 || resultCode == Activity.RESULT_OK
        return when(requestCode) {

            SALE_WEB->{

                    val saleResponse = Gson().fromJson(Gson().toJson(incomingMessage?.data), SaleResponse::class.java)
                    saleResponse.success =success
                    saleResponse.sdkVersion = BuildConfig.VERSION_NAME
                    return saleResponse
                    }

               /* SaleResponse(success, ticket?.message?:"",ticket?.authCode,ticket?.spanRoute
                ,ticket?.orderId, ticket?.folioNumber, ticket.bin,ticket.authAmoiunt,ticket.cardTypeName,ticket.affiliation,ticket.transDate,
                ticket.cardNature,
                transType = ticket.transType,
                reprintModule = ticket.reprintModule)*/

            CANCEL_SALE_WEB->{
                val cancelResponse =   Gson().fromJson(Gson().toJson(incomingMessage?.data), CancelResponse::class.java)
                cancelResponse.success =success
                cancelResponse.sdkVersion = BuildConfig.VERSION_NAME
                return cancelResponse

            }
            REPRINT_SALE_WEB-> {
                val reprintResponse =   Gson().fromJson(Gson().toJson(incomingMessage?.data), ReprintResponse::class.java)
                reprintResponse.success =success
                reprintResponse.sdkVersion = BuildConfig.VERSION_NAME
                return reprintResponse


            }
            PRINT_SALE-> PrintResponse(success, ticket?.message?:"")
            CHECK_STATUS -> {

                val reprintResponse =   Gson().fromJson(Gson().toJson(incomingMessage?.data), ReprintResponse::class.java)
                reprintResponse.success =success
                return reprintResponse
            }
            SETTLEMENT_REQUEST -> {


                returnBaseResponse(incomingMessage)

            }
            ENABLE_NAVIGATION -> BaseResponse(success, "Proceso terminado con éxito")
            ENABLE_STATUS_BAR -> BaseResponse(success, "Proceso terminado con éxito")
            SHOW_NAV_BAR -> BaseResponse(success, "Proceso terminado con éxito")
            SHOW_STATUS_BAR ->{ BaseResponse(success, "Proceso terminado con éxito")}
            READ_NFC_REQUEST -> {
                val readRep =   Gson().fromJson(responseNFC, ReadResponse::class.java)
                readRep.message ="Completado"
                return readRep
            }
            READ_MAGNETIC_REQUEST -> {
                return Gson().fromJson(response, ReadResponse::class.java)
            }
            GET_STORE_ID -> {

                val StoreResponse = Gson().fromJson(Gson().toJson(incomingMessage?.data), StoreResponse::class.java)
                if(StoreResponse.storeId == ""){
                    LogUtils.d("incomingStoreWHENIN",Gson().toJson(StoreResponse.storeId))
                    return StoreResponse(false,"Inicia sesion en la aplicacion","")
                }else{
                    StoreResponse.success = success
                    StoreResponse.message = "Todo bien"
                    LogUtils.d("incomingStoreWHEN",Gson().toJson(StoreResponse))
                    return StoreResponse
                }

            }
            else -> BaseResponse(false, "Ocurrió un error al procesar la transacción.")
        }
    }


    private fun returnBaseResponse(incomingMessage:IncomingMessage?):BaseResponse{
        val data = Gson().toJson(incomingMessage?.data)
        if(data!=null){
            val dataObject = JSONObject(data)
            val message = dataObject.optString("message")
            val code = dataObject.getInt("code") ==0
            return BaseResponse(code, message)

        }else{
            return    BaseResponse(false, "Ocurrió un error al procesar la infomación.")
        }
    }


    override fun createPage(): IPage {
        val page = smartPage.createPage()
        page.lineSpaceAdjustment = -5
        return page
    }

    private fun pageToBase64(data: IPage) :String {
        val bmp = smartPage.pageToBitmap(data, PAGE_WIDTH)
        val stream = ByteArrayOutputStream()
        bmp.compress(Bitmap.CompressFormat.PNG, 50, stream)
        val byteArray = stream.toByteArray()

        LogUtils.d("pageToBase64",byteArray.size)
        bmp.recycle()
        return String(Base64.encode(byteArray, Base64.DEFAULT))
    }

    private fun replaceColor(src: Bitmap): Bitmap {
        val width = src.width
        val height = src.height
        val pixels = IntArray(width * height)
        src.getPixels(pixels, 0, width, 0, 0, width, height)
        for (x in pixels.indices) {
            pixels[x] = (pixels[x] shl 8 and -0x1000000).inv() and Color.BLACK
        }
        return Bitmap.createBitmap(pixels, width, height, Bitmap.Config.ARGB_8888)
    }

    private fun bitmapToBase64(data:Bitmap):String{
        val byteArrayOutputStream = ByteArrayOutputStream()
        data.compress(Bitmap.CompressFormat.PNG,100,byteArrayOutputStream)
       return  Base64.encodeToString(byteArrayOutputStream.toByteArray(), Base64.DEFAULT)
    }

    override fun enableNavigation(key : NavBarButtons, enable : Boolean) {
        val uri = Uri.parse(Constants.URI)
        val intent = Intent(Constants.ACTION)
        intent.data = uri

        val incomingMessage = IncomingMessage(5, NavigationRequest("mx.com.netpay.demosdk", key.idReport, enable))
        intent.putExtra("INCOMING_MESSAGE", Gson().toJson(incomingMessage))

        intent.putExtra("KEY_SELECTED", key.idReport)
        intent.putExtra("ENABLE", enable)
        activity.startActivityForResult(intent, ENABLE_NAVIGATION)
        activity.overridePendingTransition(0, 0)
    }

    override fun enableStatusBar(enable: Boolean) {
        val uri = Uri.parse(Constants.URI)
        val intent = Intent(Constants.ACTION)
        intent.data = uri

        val incomingMessage = IncomingMessage(6, NavigationRequest("mx.com.netpay.demosdk", 0, enable))
        intent.putExtra("INCOMING_MESSAGE", Gson().toJson(incomingMessage))

        intent.putExtra("ENABLE", enable)
        activity.startActivityForResult(intent, ENABLE_STATUS_BAR)
        activity.overridePendingTransition(0, 0)
    }

    override fun showNavBar(enable: Boolean) {
        val uri = Uri.parse(Constants.URI)
        val intent = Intent(Constants.ACTION)
        intent.data = uri

        val incomingMessage = IncomingMessage(7, NavigationRequest("mx.com.netpay.demosdk", 0, enable))
        intent.putExtra("INCOMING_MESSAGE", Gson().toJson(incomingMessage))

        intent.putExtra("ENABLE", enable)
        activity.startActivityForResult(intent, SHOW_NAV_BAR)
        activity.overridePendingTransition(0, 0)
    }

    override fun showStatusBar(enable: Boolean) {
        val uri = Uri.parse(Constants.URI)
        val intent = Intent(Constants.ACTION)
        intent.data = uri

        val incomingMessage = IncomingMessage(8, NavigationRequest("mx.com.netpay.demosdk", 0, enable))
        intent.putExtra("INCOMING_MESSAGE", Gson().toJson(incomingMessage))

        intent.putExtra("ENABLE", enable)
        activity.startActivityForResult(intent, SHOW_STATUS_BAR)
        activity.overridePendingTransition(0, 0)
    }

    override fun readMagneticCardBand() {
        val uri = Uri.parse(Constants.URI)
        val intent = Intent(Constants.ACTION)
        intent.data = uri

        val incomingMessage = IncomingMessage(READ_M_OPTON, ReadRequest("mx.com.netpay.demosdk"))
        intent.putExtra("INCOMING_MESSAGE", Gson().toJson(incomingMessage))
        intent.addFlags(
                Intent.FLAG_ACTIVITY_NEW_TASK or
                        Intent.FLAG_ACTIVITY_CLEAR_TASK
            )
        activity.startActivityForResult(intent, SHOW_STATUS_BAR)
        activity.overridePendingTransition(0, 0)
    }
override fun getStoreID() {
    try {
        val uri = Uri.parse(Constants.URI)
        val intent = Intent(Constants.ACTION)
        intent.data = uri

        val incomingMessage = IncomingMessage(GET_STORE_ID - 100, ReadRequest("mx.com.netpay.demosdk"))
        intent.putExtra("INCOMING_MESSAGE", Gson().toJson(incomingMessage))
        LogUtils.d("incomingStoreIN",incomingMessage)
        activity.startActivityForResult(intent, GET_STORE_ID)
        activity.overridePendingTransition(0, 0)
    }catch ( e:Exception){

    }

}

    override fun doRead(request: ReadNFCRequest) {
        request.checkArgs()
        val uri = Uri.parse(Constants.URINFC)
        val intent = Intent(Constants.ACTIONNFC)
        intent.data = uri
        try {
            val incomingMessage = IncomingMessage(request.getIdReport(), request)

            val message = Gson().toJson(incomingMessage)
            Message.set(activity, message)
            val requestCode = request.getIdReport() + 100
            val activities: List<ResolveInfo> = activity.packageManager.queryIntentActivities(
                intent,
                PackageManager.MATCH_DEFAULT_ONLY
            )
            val isIntentSafe: Boolean = activities.isNotEmpty()
            if (isIntentSafe) {

                activity.startActivityForResult(intent, requestCode)
                activity.overridePendingTransition(0, 0)
            } else {
                Toast.makeText(activity, "Smart Pinpad no instalada", Toast.LENGTH_LONG)
                    .show()
            }
        }catch (activityNotFound: ActivityNotFoundException) {
            Toast.makeText(activity, "Smart Pinpad no instalada", Toast.LENGTH_LONG)
                .show()
        }catch (exception:Exception){
            Toast.makeText(activity, "Smart Pinpad no instalada", Toast.LENGTH_LONG)
                .show()
        }
    }

    override fun doRead(request: ReadNFCAuthRequest) {
        request.checkArgs()
        val uri = Uri.parse(Constants.URINFC)
        val intent = Intent(Constants.ACTIONNFC)
        intent.data = uri
        try {
            val incomingMessage =  IncomingMessage(request.getIdReport(), request)

            val message = Gson().toJson(incomingMessage)
            Message.set(activity,message)
            val requestCode = request.getIdReport() + 100
            val activities: List<ResolveInfo> = activity.packageManager.queryIntentActivities(
                intent,
                PackageManager.MATCH_DEFAULT_ONLY
            )
            val isIntentSafe: Boolean = activities.isNotEmpty()
            if (isIntentSafe) {

                request.password.apply {
                    intent.putExtra("PASSWORD",request.password)
                }
                request.block.apply {
                    intent.putExtra("BLOCK",request.password)
                }

                intent.putExtra("AUTH",true)

                activity.startActivityForResult(intent, requestCode)
                activity.overridePendingTransition(0, 0)

            } else {
                Toast.makeText(activity, "Smart Pinpad no instalada", Toast.LENGTH_LONG)
                    .show()
            }

        } catch(activityNotFound: ActivityNotFoundException){
            Toast.makeText(activity, "Smart Pinpad no instalada", Toast.LENGTH_LONG)
                .show()
        }catch (exception:Exception){
            Toast.makeText(activity, "Smart Pinpad no instalada", Toast.LENGTH_LONG)
                .show()

        }

    }
    /*override fun doTransWithTip(request: BaseRequest) {
        request.checkArgs()
        val uri = Uri.parse(Constants.URI)
        val intent = Intent(Constants.ACTION)f
        intent.data = uri

        val incomingMessage = if(request is PrintRequest) {
            val bmp = smartPage.pageToBitmap(request.page, 365)
            val stream = ByteArrayOutputStream()
            bmp.compress(Bitmap.CompressFormat.PNG, 100, stream)
            val byteArray = stream.toByteArray()
            bmp.recycle()
            IncomingMessage(request.getIdReport(), String(Base64.encode(byteArray, Base64.DEFAULT)))
        } else {
            IncomingMessage(request.getIdReport(),request)
        }
        intent.putExtra("displayScreenTip", true)
        intent.putExtra("INCOMING_MESSAGE", Gson().toJson(incomingMessage))
        val requestCode = request.getIdReport() + 100
        activity.startActivityForResult(intent, requestCode)
    }*/
}